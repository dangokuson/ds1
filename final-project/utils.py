import os

import pandas as pd

SOURCE_FILES = {
    'loans': os.path.join('data', 'loan_table.csv'),
    'borrowers': os.path.join('data', 'borrower_table.csv'),
}


def parse_date(arg, fmt):
    """Convert argument to datetime."""

    return pd.to_datetime(arg, format=fmt, exact=True)


def load_loans():
    df = pd.read_csv(SOURCE_FILES['loans'])

    # Rename columns `date` to `loan_request_date` to avoid duplication name of column
    df = df.rename(columns={
        'date': 'loan_request_date',
    })
    df['loan_request_date'] = (df.loan_request_date.apply(lambda arg: parse_date(arg, '%Y-%m-%d')))
    df['loan_purpose'] = df.loan_purpose.astype('category')
    return df


def load_borrowers():
    return pd.read_csv(SOURCE_FILES['borrowers'])


def get_dummies(data, columns):
    """Convert categorical variable into dummy/indicator variables."""

    df_out = data

    del_columns = []
    for column in columns:
        if df_out[column].isnull().sum() > 0:
            df_out = pd.get_dummies(df_out, columns=[column], prefix_sep='__', dummy_na=True, drop_first=False)

            # Mark for removing
            del_columns.append(column)
        else:
            df_out = pd.get_dummies(df_out, columns=[column], prefix_sep='__', dummy_na=False, drop_first=True)

    # Drop the NaN column
    for del_column in del_columns:
        del df_out['{}__nan'.format(del_column)]

    return df_out
