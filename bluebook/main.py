import pandas as pd
import re
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from IPython.display import display
import numpy as np
import math
from sklearn import metrics
from pandas.api.types import is_string_dtype, is_numeric_dtype
import matplotlib.pyplot as plt
from sklearn.ensemble import forest
import scipy
from scipy.cluster import hierarchy as hc
from utils import *

PATH = "data/"
# df_raw = pd.read_csv(f'{PATH}Train.csv', low_memory=False, parse_dates=['saledate'])
# df_raw.head()
#
# os.makedirs('tmp', exist_ok=True)
# df_raw.to_feather('tmp/bulldozers-raw')
df_raw = pd.read_feather('tmp/bulldozers-raw')

print(df_raw.head())

# Change SalePrice to log because the evaluation is for RMSLE
df_raw.SalePrice = np.log(df_raw.SalePrice)
# Change dates to date parts
add_datepart(df_raw, 'saledate')
# Add a column for age of bulldozer
df_raw['age'] = df_raw['saleYear'] - df_raw['YearMade']

# Change string variables to category type
train_cats(df_raw)
# Specify order for variable UsageBand and change to codes
df_raw.UsageBand.cat.set_categories(['High', 'Medium', 'Low'], ordered=True, inplace=True)
df_raw.UsageBand = df_raw.UsageBand.cat.codes
# Change categories to code and missing values to 0, replace missing numeric values with median,
# add column to indicate replaced missing values and separate the dependent variable as a separate df
df, y, nas = proc_df(df_raw, 'SalePrice')

print(df.head())
print(df.shape)


def split_vals(a,n):
    return a[:n].copy(), a[n:].copy()


# Split the dataset into training and validation sets. Use 12,000 as the validation set
n_valid = 12000  # same as Kaggle's test set size
n_trn = len(df)-n_valid
raw_train, raw_valid = split_vals(df_raw, n_trn) #for using unprocessed data if needed.
X_train, X_valid = split_vals(df, n_trn)
y_train, y_valid = split_vals(y, n_trn)


def rmse(x,y):
    return math.sqrt(((x-y)**2).mean())


def print_score(m):
    res = [rmse(m.predict(X_train), y_train), rmse(m.predict(X_valid), y_valid),
                m.score(X_train, y_train), m.score(X_valid, y_valid)]
    if hasattr(m, 'oob_score_'): res.append(m.oob_score_)
    print(res)


def get_oob(df):
    rf = RandomForestRegressor(n_estimators=40, max_features=0.6, n_jobs=-1, oob_score=True)
    x, _ = split_vals(df, n_trn)
    rf.fit(x, y_train)
    return rf.oob_score_


# Run RandomForestRegressor
rf = RandomForestRegressor(n_estimators=40, n_jobs=-1)
rf.fit(X_train, y_train)
print_score(rf)

# Test with test set
test_df = pd.read_csv(f'{PATH}Test.csv', low_memory=False, parse_dates=['saledate'])
add_datepart(test_df, 'saledate')
test_df['age'] = test_df['saleYear'] - test_df['YearMade']
train_cats(test_df)
test_df.UsageBand.cat.set_categories(['High', 'Medium', 'Low'], ordered=True, inplace=True)
test_df.UsageBand = test_df.UsageBand.cat.codes
test_df, _, _ = proc_df(test_df, na_dict=nas)
output_df = pd.DataFrame({
    'SalesId': test_df.SalesID,
    'SalePrice': rf.predict(test_df)
})
output_df.to_csv('bulldozer_test_result.csv')
