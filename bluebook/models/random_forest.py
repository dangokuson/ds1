import os
import re

import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.image as mpimg

import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.model_selection import (
    cross_val_score, ShuffleSplit, train_test_split, GridSearchCV)
from sklearn.utils import shuffle
from sklearn.ensemble import RandomForestRegressor
from nmlu.qinspect.nb import df_peek, df_display_all
from nmlu.qinspect.common import df_types_and_stats

PATH = "../data"

# load training data
df_raw = pd.read_csv(f'{PATH}/Train.csv', low_memory=False,
                     parse_dates=["saledate"])
df_peek(df_raw)

# save to disk
os.makedirs('tmp', exist_ok=True)
df_raw.to_feather('tmp/bulldozers-raw')

# # load test data (what's called "validation" on Kaggle to separate it from final test data)
# df_test_raw = pd.read_csv(
#     f'{PATH}/Valid.csv', low_memory=False, parse_dates=["saledate"])
# df_peek(df_test_raw)
#
# # load correct results for test data ("validation solution" on Kaggle)
# df_test_results = pd.read_csv(
#     f'{PATH}/ValidSolution.csv')
# df_peek(df_test_results)