import pandas as pd, pickle, sys, itertools, string, sys, re, datetime, time, shutil, copy
from pandas.api.types import is_string_dtype, is_numeric_dtype
import numpy as np
from IPython.display import display


PATH = "data/"


def draw_sine():
    df_raw = pd.read_csv(f'{PATH}Train.csv', low_memory=False, parse_dates=["saledate"])
    return df_raw